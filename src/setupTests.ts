// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom'
import numeral from 'numeral'

numeral.zeroFormat('0')
numeral.defaultFormat('0,0.00')

beforeEach(() => {
  jest.clearAllMocks()
})
