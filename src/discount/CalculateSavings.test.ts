import CalculateSavings from './CalculateSavings'
import Product from '../product/Product'

describe('CalculateSavings', () => {
  const faceMask = new Product(1, 'Face Mask', 2.5)
  const toiletPaper = new Product(2, 'Toilet Paper', 0.65)
  const handSanitizer = new Product(3, 'Hand Sanitizer', 19.99)

  it('should return 0 product has no discount', () => {
    const savings = CalculateSavings([handSanitizer])

    expect(savings).toHaveLength(0)
  })

  it('should should calculate correct savings for face masks', () => {
    const savings = CalculateSavings([faceMask, faceMask, faceMask])

    expect(savings).toHaveLength(1)
    expect(savings[0].title).toBe('Face Masks 2 for £4')
  })

  it('should should calculate correct savings for toilet papers', () => {
    const savings = CalculateSavings([
      toiletPaper,
      toiletPaper,
      toiletPaper,
      toiletPaper,
      toiletPaper,
      toiletPaper,
      toiletPaper,
    ])

    expect(savings).toHaveLength(1)
    expect(savings[0].title).toBe('Toilet Paper 6 for 5')
  })
})
