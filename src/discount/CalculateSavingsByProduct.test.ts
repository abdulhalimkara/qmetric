import CalculateSavingsByProduct from './CalculateSavingsByProduct'
import Product from '../product/Product'

describe('CalculateSavingsByProduct', () => {
  const faceMask = new Product(1, 'Face Mask', 2.5)
  const toiletPaper = new Product(2, 'Toilet Paper', 0.65)
  const handSanitizer = new Product(3, 'Hand Sanitizer', 19.99)

  it('should return 0 product has no discount', () => {
    const saving = CalculateSavingsByProduct(handSanitizer, 2)

    expect(saving.amount).toBe(0)
  })

  it('should should calculate correct savings for face masks', () => {
    const saving1 = CalculateSavingsByProduct(faceMask, 3)
    const saving5 = CalculateSavingsByProduct(faceMask, 10)

    expect(saving1.amount).toBe(1)
    expect(saving1.title).toBe('Face Masks 2 for £4')
    expect(saving5.amount).toBe(5)
    expect(saving5.title).toBe('Face Masks 2 for £4')
  })

  it('should should calculate correct savings for toilet papers', () => {
    const saving65 = CalculateSavingsByProduct(toiletPaper, 10)
    const saving13 = CalculateSavingsByProduct(toiletPaper, 12)

    expect(saving65.amount).toBe(0.65)
    expect(saving65.title).toBe('Toilet Paper 6 for 5')
    expect(saving13.amount).toBe(1.3)
    expect(saving13.title).toBe('Toilet Paper 6 for 5')
  })
})
