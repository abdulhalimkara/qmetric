import Product from '../product/Product'
import CalculateSavingsByProduct, {
  SavingItem,
} from './CalculateSavingsByProduct'

type Quantities = Record<number, { product: Product; quantity: number }>

export default function CalculateSavings(products: Product[]) {
  const quantities = products.reduce((acc: Quantities, product) => {
    if (!acc[product.id]) {
      acc[product.id] = { product, quantity: 1 }
    } else {
      acc[product.id].quantity++
    }

    return acc
  }, {})

  const savings: SavingItem[] = []
  Object.values(quantities).forEach(({ product, quantity }) => {
    const saving = CalculateSavingsByProduct(product, quantity)

    if (saving.amount > 0) {
      savings.push(saving)
    }
  })

  return savings
}
