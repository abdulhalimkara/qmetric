import PriceDiscount from './PriceDiscount'

describe('PriceDiscount', () => {
  it('should return 0 when quantity < discountQuantity', () => {
    const saving = PriceDiscount(1, 2.5, 2, 4)

    expect(saving).toBe(0)
  })

  it('should should calculate correct savings', () => {
    const saving1 = PriceDiscount(3, 2.5, 2, 4)
    const saving5 = PriceDiscount(10, 2.5, 2, 4)

    expect(saving1).toBe(1)
    expect(saving5).toBe(5)
  })
})
