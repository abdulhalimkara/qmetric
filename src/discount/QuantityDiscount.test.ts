import QuantityDiscount from './QuantityDiscount'

describe('QuantityDiscount', () => {
  it('should throw error for invalid quantities', () => {
    expect(() => {
      QuantityDiscount(10, 2.5, 6, 7)
    }).toThrowError(/Invalid quantity amounts/i)
  })

  it('should return 0 when quantity < quantityOrig', () => {
    const saving = QuantityDiscount(1, 2.5, 6, 5)

    expect(saving).toBe(0)
  })

  it('should should calculate correct savings', () => {
    const saving25 = QuantityDiscount(6, 2.5, 6, 5)
    const saving5 = QuantityDiscount(17, 2.5, 6, 5)

    expect(saving25).toBe(2.5)
    expect(saving5).toBe(5)
  })
})
