import PriceDiscount from './PriceDiscount'
import QuantityDiscount from './QuantityDiscount'
import Product from '../product/Product'

interface Discount {
  func: typeof PriceDiscount | typeof QuantityDiscount
  args: number[]
  title: string
}

// Ideally, this should be implemented in backend
const productToDiscount: Record<number, Discount> = {
  1: {
    func: PriceDiscount,
    args: [2, 4],
    title: 'Face Masks 2 for £4',
  },
  2: {
    func: QuantityDiscount,
    args: [6, 5],
    title: 'Toilet Paper 6 for 5',
  },
}

export interface SavingItem {
  amount: number
  title: string
}

export default function CalculateSavingsByProduct(
  product: Product,
  quantity: number
): SavingItem {
  const discount = productToDiscount[product.id]

  if (!discount) {
    return { amount: 0, title: '' }
  }

  // @ts-ignore
  const amount = discount.func(quantity, product.price, ...discount.args)
  const { title } = discount

  return { amount, title }
}
