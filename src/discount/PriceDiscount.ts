import numeral from 'numeral'

export default function PriceDiscount(
  quantity: number,
  unitPrice: number,
  discountQuantity: number,
  discountPrice: number
) {
  const quantityNumeral = numeral(quantity)

  const discountCount = Math.floor(
    quantityNumeral.divide(discountQuantity).value()
  )

  if (discountCount < 1) {
    return 0
  }

  const normalTotalPriceNumeral = numeral(discountQuantity).multiply(unitPrice)
  const discountUnitAmount = normalTotalPriceNumeral
    .subtract(discountPrice)
    .value()
  const discountAmount = numeral(discountCount)
    .multiply(discountUnitAmount)
    .value()

  return discountAmount
}
