import numeral from 'numeral'

export default function QuantityDiscount(
  quantity: number,
  unitPrice: number,
  quantityOrig: number,
  quantityFor: number
) {
  if (quantityOrig <= quantityFor) {
    throw new Error('Invalid quantity amounts')
  }

  const quantityNumeral = numeral(quantity)
  const quantityOrigNumeral = numeral(quantityOrig)

  const discountCount = Math.floor(quantityNumeral.divide(quantityOrig).value())

  if (discountCount < 1) {
    return 0
  }

  const quantityDiff = quantityOrigNumeral.subtract(quantityFor)
  const discountUnitAmount = quantityDiff.multiply(unitPrice)
  const discountAmount = discountUnitAmount.multiply(discountCount).value()

  return discountAmount
}
