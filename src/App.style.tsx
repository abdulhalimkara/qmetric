import styled from 'styled-components'

export const StyledApp = styled.div`
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const StyledContent = styled.div`
  max-width: 100%;
  width: 800px;
  padding: 1rem;
`
