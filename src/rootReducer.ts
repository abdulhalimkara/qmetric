import { combineReducers } from 'redux'
import basket from './basket/reducers'

const rootReducer = combineReducers({
  basket,
  // new reducers can be added here
})

export type RootStateType = ReturnType<typeof rootReducer>

export default rootReducer
