import React from 'react'
import Card from 'react-bootstrap/Card'
import ListGroup from 'react-bootstrap/ListGroup'

import Product from '../product/Product'
import BasketItem from './BasketItem'

interface Props {
  products: Product[]
}

const BasketBody: React.FC<Props> = ({ products }) => {
  return (
    <Card.Body>
      <ListGroup variant="flush" as="ul">
        {products.map((product, i) => (
          <BasketItem key={`${i}-${product.id}`} product={product} index={i} />
        ))}
      </ListGroup>
    </Card.Body>
  )
}

export default BasketBody
