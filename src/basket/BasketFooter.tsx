import React from 'react'
import Card from 'react-bootstrap/Card'
import Table from 'react-bootstrap/Table'
import numeral from 'numeral'

import { SavingItem } from '../discount/CalculateSavingsByProduct'

interface Props {
  subTotal: number
  savings: SavingItem[]
}

const BasketFooter: React.FC<Props> = ({ subTotal, savings }) => {
  const subTotalFormatted = numeral(subTotal).format()
  const totalSavings = savings.reduce(
    (total, saving) => total.add(saving.amount),
    numeral(0)
  )
  const totalSavingsFormatted = totalSavings.format()
  const totalToPayFormatted = numeral(subTotal)
    .subtract(totalSavings.value())
    .format()

  return (
    <Card.Footer>
      <Table borderless size="sm">
        <tbody>
          <tr>
            <td>Sub-total</td>
            <td className="text-right">{subTotalFormatted}</td>
          </tr>
          {savings.length > 0 && (
            <>
              <tr>
                <td className="text-muted">Savings</td>
                <td> </td>
              </tr>
              {savings.map((saving) => (
                <tr data-testid="saving" key={saving.title}>
                  <td>{saving.title}</td>
                  <td className="text-right">
                    -{numeral(saving.amount).format()}
                  </td>
                </tr>
              ))}
              <tr className="border-top border-bottom">
                <td>Total savings</td>
                <td className="text-right">-{totalSavingsFormatted}</td>
              </tr>
            </>
          )}
          <tr>
            <td>Total to Pay</td>
            <td className="text-right">{totalToPayFormatted}</td>
          </tr>
        </tbody>
      </Table>
    </Card.Footer>
  )
}

export default BasketFooter
