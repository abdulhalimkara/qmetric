import React from 'react'

import { fireEvent, render } from '../test-utils'
import BasketItem from './BasketItem'
import Product from '../product/Product'
import { removeProduct } from './actions'

jest.mock('./actions', () => ({
  removeProduct: jest.fn(),
}))

describe('BasketItem', () => {
  const product = new Product(1, 'Face Mask', 2.5)

  it('should render item', () => {
    const { getByText } = render(<BasketItem product={product} index={0} />)
    const element = getByText(product.name)

    expect(element).toBeInTheDocument()
  })

  it('should call removeProduct action', () => {
    const index = 1
    ;(removeProduct as jest.Mock).mockReturnValue({
      type: 'REMOVE_PRODUCT',
      index,
    })
    const { getByRole } = render(<BasketItem product={product} index={index} />)
    const btn = getByRole('button')

    fireEvent.click(btn)

    expect(removeProduct).toHaveBeenCalledWith(index)
  })
})
