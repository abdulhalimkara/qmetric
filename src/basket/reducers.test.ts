import reducers from './reducers'
import { ADD_PRODUCT, BASKET_ACTIONS_TYPE, REMOVE_PRODUCT } from './actions'
import Product from '../product/Product'

describe('basket reducers', () => {
  const faceMask = new Product(1, 'Face Mask', 2.5)
  const toiletPaper = new Product(2, 'Toilet Paper', 0.65)
  const initialState: { products: Product[] } = { products: [] }

  it('should handle initial state', () => {
    expect(reducers(undefined, {} as BASKET_ACTIONS_TYPE)).toEqual(initialState)
  })

  it('should handle ADD_PRODUCT', () => {
    const products = reducers(initialState, {
      type: ADD_PRODUCT,
      product: faceMask,
    } as BASKET_ACTIONS_TYPE)

    const products2 = reducers({ products: [faceMask] }, {
      type: ADD_PRODUCT,
      product: toiletPaper,
    } as BASKET_ACTIONS_TYPE)

    expect(products).toEqual({ products: [faceMask] })
    expect(products2).toEqual({ products: [faceMask, toiletPaper] })
  })

  it('should handle REMOVE_PRODUCT', () => {
    const products = reducers({ products: [faceMask] }, {
      type: REMOVE_PRODUCT,
      index: 0,
    } as BASKET_ACTIONS_TYPE)

    expect(products).toEqual(initialState)
  })
})
