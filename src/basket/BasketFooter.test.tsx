import React from 'react'

import { render } from '../test-utils'
import BasketFooter from './BasketFooter'

describe('BasketFooter', () => {
  const subTotal = 12.4
  const subTotalFormatted = '12.40'
  const totalSavingsFormatted = '-1.60'
  const totalToPayFormatted = '10.80'
  const savings = [
    {
      title: 'Face Masks 2 for £4',
      amount: 1,
    },
    {
      title: 'Toilet Paper 6 for 5',
      amount: 0.6,
    },
  ]

  it('should render formatted subtotal', () => {
    const { getByText } = render(
      <BasketFooter subTotal={subTotal} savings={savings} />
    )
    const element = getByText(subTotalFormatted)

    expect(element).toBeInTheDocument()
  })

  it('should render savings with formatted amounts', () => {
    const { getAllByTestId } = render(
      <BasketFooter subTotal={subTotal} savings={savings} />
    )
    const elements = getAllByTestId('saving')

    expect(elements.length).toBe(savings.length)
    expect(elements[0]).toHaveTextContent('1.00')
    expect(elements[1]).toHaveTextContent('0.60')
  })

  it('should not render savings rows', () => {
    const { queryByText, queryAllByTestId } = render(
      <BasketFooter subTotal={subTotal} savings={[]} />
    )
    const element = queryByText('Savings')
    const elements = queryAllByTestId('saving')

    expect(element).toBeNull()
    expect(elements.length).toBe(0)
  })

  it('should render formatted total savings', () => {
    const { getByText } = render(
      <BasketFooter subTotal={subTotal} savings={savings} />
    )
    const element = getByText(/Total savings/i)

    expect(element.parentNode).toHaveTextContent(totalSavingsFormatted)
  })

  it('should render formatted total to pay', () => {
    const { getByText } = render(
      <BasketFooter subTotal={subTotal} savings={savings} />
    )
    const element = getByText(/Total to Pay/i)

    expect(element.parentNode).toHaveTextContent(totalToPayFormatted)
  })
})
