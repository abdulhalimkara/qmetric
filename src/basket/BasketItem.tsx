import React from 'react'
import { connect, ConnectedProps } from 'react-redux'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import ListGroupItem from 'react-bootstrap/ListGroupItem'
import Button from 'react-bootstrap/Button'

import Product from '../product/Product'
import { removeProduct as removeProductAction } from './actions'

const BasketItem: React.FC<Props> = ({ product, index, removeProduct }) => {
  return (
    <ListGroupItem className="pl-0 pr-0" as="li">
      <Row className="align-items-center no-gutters">
        <Col>{product.name}</Col>
        <Col xs="auto" className="font-weight-bold ml-2 mr-2 text-right">
          £ {product.priceFormatted}
        </Col>
        <Col xs="auto">
          <Button
            variant="link"
            className="text-muted"
            size="sm"
            aria-label={`Remove ${product.name}`}
            onClick={() => removeProduct(index)}
          >
            x
          </Button>
        </Col>
      </Row>
    </ListGroupItem>
  )
}

const mapState = null
const mapDispatch = { removeProduct: removeProductAction }

const connector = connect(mapState, mapDispatch)

type PropsFromRedux = ConnectedProps<typeof connector>

interface Props extends PropsFromRedux {
  product: Product
  index: number
}

export default connector(BasketItem)
