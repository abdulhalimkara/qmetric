import { addProduct, ADD_PRODUCT } from './actions'
import Product from '../product/Product'

describe('basket actions', () => {
  const product = new Product(1, 'Face Mask', 2.5)

  it('should create ADD_PRODUCT action', () => {
    expect(addProduct(product)).toEqual({
      type: ADD_PRODUCT,
      product,
    })
  })
})
