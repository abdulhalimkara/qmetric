import { ADD_PRODUCT, BASKET_ACTIONS_TYPE, REMOVE_PRODUCT } from './actions'
import Product from '../product/Product'

const initialState: { products: Product[] } = { products: [] }

const reducers = (state = initialState, action: BASKET_ACTIONS_TYPE) => {
  switch (action.type) {
    case ADD_PRODUCT:
      return { ...state, products: [...state.products, action.product] }
    case REMOVE_PRODUCT:
      return {
        ...state,
        products: [
          ...state.products.slice(0, action.index),
          ...state.products.slice(action.index + 1),
        ],
      }
    default:
      return state
  }
}

export default reducers
