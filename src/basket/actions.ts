import Product from '../product/Product'

export const ADD_PRODUCT = 'basket/ADD_PRODUCT'
export const REMOVE_PRODUCT = 'basket/REMOVE_PRODUCT'

export const addProduct = (product: Product) => ({
  type: ADD_PRODUCT,
  product,
})

export const removeProduct = (index: number) => ({
  type: REMOVE_PRODUCT,
  index,
})

export type BASKET_ACTIONS_TYPE = ReturnType<typeof addProduct> &
  ReturnType<typeof removeProduct>
