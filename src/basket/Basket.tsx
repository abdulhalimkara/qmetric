import React from 'react'
import { connect, ConnectedProps } from 'react-redux'
import Card from 'react-bootstrap/Card'
import numeral from 'numeral'

import BasketBody from './BasketBody'
import BasketFooter from './BasketFooter'
import { RootStateType } from '../rootReducer'
import { SavingItem } from '../discount/CalculateSavingsByProduct'
import CalculateSavings from '../discount/CalculateSavings'

const Basket = ({ products }: Props) => {
  // I am using numeral here due to JS floating point issue: 2.3 + 2.4 = 4.699999999999999
  const subTotal = products
    .reduce((total, product) => total.add(product.price), numeral(0))
    .value()

  const savings: SavingItem[] = CalculateSavings(products)

  return (
    <Card>
      {products.length > 0 && (
        <>
          <BasketBody products={products} />
          <BasketFooter subTotal={subTotal} savings={savings} />
        </>
      )}
      {products.length === 0 && (
        <div className="p-3 text-center">Your basket is empty</div>
      )}
    </Card>
  )
}

const mapState = (state: RootStateType) => state.basket
const mapDispatch = {}

const connector = connect(mapState, mapDispatch)

type Props = ConnectedProps<typeof connector>

export default connector(Basket)
