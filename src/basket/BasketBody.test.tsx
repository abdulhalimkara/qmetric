import React from 'react'

import { render } from '../test-utils'
import BasketBody from './BasketBody'
import Product from '../product/Product'

describe('BasketBody', () => {
  const products = [new Product(1, 'Face Mask', 2.5)]

  it('should render products', () => {
    const { getAllByRole } = render(<BasketBody products={products} />)
    const elements = getAllByRole('listitem')

    expect(elements.length).toBe(products.length)
    expect(elements[0]).toHaveTextContent(products[0].name)
  })
})
