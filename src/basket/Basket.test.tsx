import React from 'react'

import { render } from '../test-utils'
import Basket from './Basket'
import Product from '../product/Product'

describe('Basket', () => {
  const subTotalFormatted = '3.10'
  const products = [
    new Product(1, 'Face Mask', 2.5),
    new Product(2, 'Toilet Paper', 0.6),
  ]

  it('should render no items message', () => {
    const { getByText } = render(<Basket />)
    const element = getByText(/Your basket is empty/i)

    expect(element).toBeInTheDocument()
  })

  it('should calculate sub-total properly', () => {
    const { getByText } = render(<Basket />, {
      initialState: { basket: { products } },
    })
    const element = getByText(/Sub-total/i)

    expect(element.parentNode).toHaveTextContent(subTotalFormatted)
  })

  it('should render savings', () => {
    const items = [
      new Product(1, 'Face Mask', 2.5),
      new Product(1, 'Face Mask', 2.5),
      new Product(2, 'Toilet Paper', 0.6),
    ]
    const { getByText } = render(<Basket />, {
      initialState: { basket: { products: items } },
    })
    const element = getByText(/Face Masks 2 for £4/i)

    expect(element.parentNode).toHaveTextContent('-1.00')
  })
})
