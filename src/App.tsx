import React from 'react'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

import ProductList from './product/ProductList'
import Product from './product/Product'
import Basket from './basket/Basket'
import { StyledApp, StyledContent } from './App.style'

const products = [
  new Product(1, 'Face Mask', 2.5),
  new Product(2, 'Toilet Paper', 0.65),
  new Product(3, 'Hand Sanitizer', 19.99),
]

function App() {
  return (
    <StyledApp>
      <StyledContent>
        <Row>
          <Col xs={12} md={6} className="mb-3">
            <ProductList products={products} />
          </Col>
          <Col xs={12} md={6}>
            <Basket />
          </Col>
        </Row>
      </StyledContent>
    </StyledApp>
  )
}

export default App
