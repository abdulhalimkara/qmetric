import React from 'react'
import { render } from '@testing-library/react'
import { createStore } from 'redux'
import { Provider } from 'react-redux'

import rootReducer from './rootReducer'

function customRender(
  ui: React.ReactElement,
  {
    initialState = {},
    store = createStore(rootReducer, initialState),
    ...options
  } = {} as any
) {
  const Wrapper: React.FC = ({ children }) => {
    return <Provider store={store}>{children}</Provider>
  }

  return render(ui, { wrapper: Wrapper, ...options })
}

export * from '@testing-library/react'
export { customRender as render }
