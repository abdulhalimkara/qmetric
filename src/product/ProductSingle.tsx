import React from 'react'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import ListGroup from 'react-bootstrap/ListGroup'
import Button from 'react-bootstrap/Button'
import { connect, ConnectedProps } from 'react-redux'

import { addProduct as addProductAction } from '../basket/actions'
import Product from './Product'

const ProductSingle: React.FC<Props> = ({ product, addProduct }) => {
  return (
    <ListGroup.Item as="li">
      <Row className="align-items-center">
        <Col>{product.name}</Col>
        <Col xs="auto">£ {product.priceFormatted}</Col>
        <Col xs="auto">
          <Button variant="secondary" onClick={() => addProduct(product)}>
            Add
          </Button>
        </Col>
      </Row>
    </ListGroup.Item>
  )
}

const mapState = null
const mapDispatch = { addProduct: addProductAction }

const connector = connect(mapState, mapDispatch)

type PropsFromRedux = ConnectedProps<typeof connector>

interface Props extends PropsFromRedux {
  product: Product
}

export default connector(ProductSingle)
