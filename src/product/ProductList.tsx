import React from 'react'
import ListGroup from 'react-bootstrap/ListGroup'
import Product from './Product'
import ProductSingle from './ProductSingle'

interface Props {
  products: Product[]
}

const ProductList: React.FC<Props> = ({ products }) => {
  return (
    <ListGroup as="ul">
      {products.map((product) => (
        <ProductSingle key={product.id} product={product} />
      ))}
    </ListGroup>
  )
}

export default ProductList
