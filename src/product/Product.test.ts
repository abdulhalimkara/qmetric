import Product from './Product'

describe('Product', () => {
  const id = 1
  const title = 'Face Mask'
  const price = 2.5
  const priceFormatted = '2.50'

  it('should format the price', async () => {
    const item = new Product(id, title, price)

    expect(item.priceFormatted).toBe(priceFormatted)
  })
})
