import React from 'react'

import { render } from '../test-utils'
import ProductList from './ProductList'

describe('ProductList', () => {
  const products = [
    {
      id: 1,
      name: 'Face Mask',
      price: 2.5,
      priceFormatted: '2.50',
    },
  ]

  it('should render products', () => {
    const { getAllByRole } = render(<ProductList products={products} />)
    const elements = getAllByRole('listitem')

    expect(elements.length).toBe(products.length)
    expect(elements[0]).toHaveTextContent(products[0].name)
  })
})
