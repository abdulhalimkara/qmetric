import numeral from 'numeral'

export default class Product {
  constructor(public id: number, public name: string, public price: number) {}

  get priceFormatted(): string {
    return numeral(this.price).format()
  }
}
