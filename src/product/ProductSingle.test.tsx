import React from 'react'

import { fireEvent, render } from '../test-utils'
import ProductSingle from './ProductSingle'
import Product from './Product'
import { addProduct } from '../basket/actions'

jest.mock('../basket/actions', () => ({
  addProduct: jest.fn(),
}))

describe('ProductSingle', () => {
  const product = new Product(1, 'Face Mask', 2.5)

  it('should render product', () => {
    const { getByText } = render(<ProductSingle product={product} />)
    const elem = getByText(product.name)

    expect(elem).toBeInTheDocument()
  })

  it('should call addProduct action', () => {
    ;(addProduct as jest.Mock).mockReturnValue({
      type: 'ADD_PRODUCT',
      product,
    })
    const { getByRole } = render(<ProductSingle product={product} />)
    const btn = getByRole('button')

    fireEvent.click(btn)

    expect(addProduct).toHaveBeenCalledWith(product)
  })
})
