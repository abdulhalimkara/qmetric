module.exports = {
  extends: ['react-app', 'prettier'],
  plugins: ['testing-library'],
  rules: {
    'no-console': 'warn',
  },
}
